﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Siatka
{
    class Node
    {
        public double X;


        public double Y;

        public double T;

        public bool Status;

        public Node(double x, double y, double t, bool status)
        {
            X = x;
            Y = y;
            T = t;
            Status = status;
        }
    }
}
