﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Siatka
{
    
    public static class GlobalData
    {
        public static double H;
        public static double B;
        public static int nH;
        public static int nB;
        public static int nh;
        public static int ne;

        public static double[] Wsp = { -1.0 / Math.Sqrt(3), 1.0 / Math.Sqrt(3) };

        public static void ReadData()
        {
            //File.WriteAllText("data.json", JsonConvert.SerializeObject(global, Formatting.Indented));
            Data nowy = JsonConvert.DeserializeObject<Data>(File.ReadAllText("data.json"));

            H = nowy.H;
            B = nowy.B;
            nH = nowy.nH;
            nB = nowy.nB;
            nh = nH * nB;
            ne = (nH - 1) * (nB - 1);

            
        }
    }
}
