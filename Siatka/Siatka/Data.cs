﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Siatka
{
    [DataContract]
    class Data
    {
        [DataMember] public double H;

        [DataMember] public double B;

        [DataMember] public int nH;

        [DataMember] public int nB;
    }
}
