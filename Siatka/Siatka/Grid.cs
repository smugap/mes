﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Siatka
{
    class Grid
    {
        public Node[] ND;
        public Element[] EL;

        public Grid()
        {
            ND = new Node[GlobalData.nh];
            EL = new Element[GlobalData.ne];
            CreateGrid();
            Print();
            PrintElements();
            Console.WriteLine("\n\n\n\n");
            UniversalElement unicat = new UniversalElement();
            double[][] test = unicat.JacobianPrzekształcenia(EL[0]);
            UniversalElement.PrintMatrix(test);
            Console.WriteLine();
        }
        //funkcja która stworzy ta siatke
        private void CreateGrid()
        {
            int index = 0;
            bool brzeg = false;
            for (int i = 0; i < GlobalData.nB; i++)
            {
                for (int j = 0; j < GlobalData.nH; j++)
                {
                    brzeg = false;
                    if (i == 0 | i == GlobalData.nB-1) brzeg = true;
                    if (j == 0 | j == GlobalData.nH-1) brzeg = true;
                    
                    ND[index] = new Node(i*GlobalData.B,(GlobalData.nH-1)*GlobalData.H - GlobalData.H*j,0,brzeg);
                    index++;
                }
            }
            //create elements from nodes
            //index = 0;
            //int help = 4;
            //for (int i = 0; i < ND.Length - GlobalData.nH - 1; i++)
            //{
            //    if (i == help)
            //    {
            //        i++;
            //        if (i >= ND.Length - GlobalData.nH - 1)
            //        {
            //            break;
            //        }
            //        help += 5;
            //    }
            //    EL[index] = new Element(ND[i + 1], ND[i + GlobalData.nH + 1], ND[i + GlobalData.nH], ND[i]);
            //    index++;
            //}

            index = GlobalData.nH - 2;
            int help = 4;
            for (int i = 0; i < ND.Length - GlobalData.nH - 1; i++)
            {
                if (i == help)
                {
                    i++;
                    index += (2 * GlobalData.nH-2);
                    if (i >= ND.Length - GlobalData.nH - 1)
                    {
                        break;
                    }
                    help += 5;
                }

                EL[index] = new Element(ND[i + 1], ND[i + GlobalData.nH + 1], ND[i + GlobalData.nH], ND[i]);
                index--;
            }


        }

        private void Print()
        {
            int index = 0;
            for (int i = 0; i < GlobalData.nB; i++)
            {
                for (int j = 0; j < GlobalData.nH; j++)
                {
                   
                    Console.WriteLine($"{ND[index].X} {ND[index].Y} {ND[index].Status} \t");
                    index++;
                }
                Console.WriteLine();
            }
        }

        private void PrintElements()
        {
            foreach (var item in EL)
            {
                Console.WriteLine($"{item.ID[0].X}|{item.ID[0].Y}\t {item.ID[1].X}|{item.ID[1].Y}\t {item.ID[2].X}|{item.ID[2].Y}\t {item.ID[3].X}|{item.ID[3].Y} ");
            }
        }
    }
}
