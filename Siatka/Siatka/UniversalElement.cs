﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Siatka
{
    class UniversalElement
    {

        private double[][] PochodnaNPoKsi = new double[4][]
        {
            new[]
            {
                (1.0 / 4.0) * (GlobalData.Wsp[0] - 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[0] - 1),
                (1.0 / 4.0) * (GlobalData.Wsp[0] + 1),
               -(1.0 / 4.0) * (GlobalData.Wsp[0] + 1)
            },
            new[]
            {
                (1.0 / 4.0) * (GlobalData.Wsp[0] - 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[0] - 1),
                (1.0 / 4.0) * (GlobalData.Wsp[0] + 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[0] + 1)
            },
            new[]
            {
                (1.0 / 4.0) * (GlobalData.Wsp[1] - 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[1] - 1),
                (1.0 / 4.0) * (GlobalData.Wsp[1] + 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[1] + 1)
            },
            new[]
            {
                (1.0 / 4.0) * (GlobalData.Wsp[1] - 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[1] - 1),
                (1.0 / 4.0) * (GlobalData.Wsp[1] + 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[1] + 1)
            }
        };

        private double[][] PochodnaNPoEta = new double[4][]
        {
            new[]
            {
                (1.0 / 4.0) * (GlobalData.Wsp[0] - 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[0] + 1),
                (1.0 / 4.0) * (GlobalData.Wsp[0] + 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[0] - 1)
            },
            new[]
            {
                (1.0 / 4.0) * (GlobalData.Wsp[1] - 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[1] + 1),
                (1.0 / 4.0) * (GlobalData.Wsp[1] + 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[1] - 1)
            },
            new[]
            {
                (1.0 / 4.0) * (GlobalData.Wsp[1] - 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[1] + 1),
                (1.0 / 4.0) * (GlobalData.Wsp[1] + 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[1] - 1)
            },
            new[]
            {
                (1.0 / 4.0) * (GlobalData.Wsp[0] - 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[0] + 1),
                (1.0 / 4.0) * (GlobalData.Wsp[0] + 1),
                -(1.0 / 4.0) * (GlobalData.Wsp[0] - 1)
            }
        };
        //double[] FunkcjeKsztaltu = new double[4];

        public UniversalElement()
        {
            //FunkcjeKsztaltu[0] = (1.0 / 4.0) * (1.0 - GlobalData.Wsp[0]) * (1.0 - GlobalData.Wsp[0]);
            //FunkcjeKsztaltu[1] = (1.0 / 4.0) * (1.0 + GlobalData.Wsp[1]) * (1.0 - GlobalData.Wsp[0]);
            //FunkcjeKsztaltu[2] = (1.0 / 4.0) * (1.0 + GlobalData.Wsp[1]) * (1.0 + GlobalData.Wsp[1]);
            //FunkcjeKsztaltu[3] = (1.0 / 4.0) * (1.0 - GlobalData.Wsp[0]) * (1.0 + GlobalData.Wsp[1]);

            

        }

        public double[][] JacobianPrzekształcenia(Element element)
        {

            double[][] jacobian1 = new double[2][]
            {
                new[]
                {
                    PochodnaNPoEta[0][0] * element.ID[0].Y + PochodnaNPoEta[0][1] * element.ID[1].Y + PochodnaNPoEta[0][2] * element.ID[2].Y + PochodnaNPoEta[0][3] * element.ID[3].Y,
                    -(PochodnaNPoKsi[0][0] * element.ID[0].Y + PochodnaNPoKsi[0][1] * element.ID[1].Y + PochodnaNPoKsi[0][2] * element.ID[2].Y + PochodnaNPoKsi[0][3] * element.ID[3].Y)

                },
                new[]
                {
                    -(PochodnaNPoEta[0][0] * element.ID[0].X + PochodnaNPoEta[0][1] * element.ID[1].X + PochodnaNPoEta[0][2] * element.ID[2].X + PochodnaNPoEta[0][3] * element.ID[3].X),
                    
                    PochodnaNPoKsi[0][0] * element.ID[0].X + PochodnaNPoKsi[0][1] * element.ID[1].X + PochodnaNPoKsi[0][2] * element.ID[2].X + PochodnaNPoKsi[0][3] * element.ID[3].X

                }

            };
            double[][] jacobian2 = new double[2][]
            {
                new[]
                {
                    PochodnaNPoEta[1][0] * element.ID[0].Y + PochodnaNPoEta[1][1] * element.ID[1].Y + PochodnaNPoEta[1][2] * element.ID[2].Y + PochodnaNPoEta[1][3] * element.ID[3].Y,
                    -(PochodnaNPoKsi[1][0] * element.ID[0].Y + PochodnaNPoKsi[1][1] * element.ID[1].Y + PochodnaNPoKsi[1][2] * element.ID[2].Y + PochodnaNPoKsi[1][3] * element.ID[3].Y)

                },
                new[]
                {
                    -(PochodnaNPoEta[1][0] * element.ID[0].X + PochodnaNPoEta[1][1] * element.ID[1].X + PochodnaNPoEta[1][2] * element.ID[2].X + PochodnaNPoEta[1][3] * element.ID[3].X),
                    PochodnaNPoKsi[1][0] * element.ID[0].X + PochodnaNPoKsi[1][1] * element.ID[1].X + PochodnaNPoKsi[1][2] * element.ID[2].X + PochodnaNPoKsi[1][3] * element.ID[3].X
                    
                }
            };
            double[][] jacobian3 = new double[2][]
            {
                new[]
                {
                    PochodnaNPoEta[2][0] * element.ID[0].Y + PochodnaNPoEta[2][1] * element.ID[1].Y + PochodnaNPoEta[2][2] * element.ID[2].Y + PochodnaNPoEta[2][3] * element.ID[3].Y,
                    -(PochodnaNPoKsi[2][0] * element.ID[0].Y + PochodnaNPoKsi[2][1] * element.ID[1].Y + PochodnaNPoKsi[2][2] * element.ID[2].Y + PochodnaNPoKsi[2][3] * element.ID[3].Y)

                },
                new[]
                {
                    -(PochodnaNPoEta[2][0] * element.ID[0].X + PochodnaNPoEta[2][1] * element.ID[1].X + PochodnaNPoEta[2][2] * element.ID[2].X + PochodnaNPoEta[2][3] * element.ID[3].X),
                    PochodnaNPoKsi[2][0] * element.ID[0].X + PochodnaNPoKsi[2][1] * element.ID[1].X + PochodnaNPoKsi[2][2] * element.ID[2].X + PochodnaNPoKsi[2][3] * element.ID[3].X
                    
                }
            };
            double[][] jacobian4 = new double[2][]
            {
                new[]
                {
                    PochodnaNPoEta[3][0] * element.ID[0].Y + PochodnaNPoEta[3][1] * element.ID[1].Y + PochodnaNPoEta[3][2] * element.ID[2].Y + PochodnaNPoEta[3][3] * element.ID[3].Y,
                    -(PochodnaNPoKsi[3][0] * element.ID[0].Y + PochodnaNPoKsi[3][1] * element.ID[1].Y + PochodnaNPoKsi[3][2] * element.ID[2].Y + PochodnaNPoKsi[3][3] * element.ID[3].Y)

                },
                new[]
                {
                    -(PochodnaNPoEta[3][0] * element.ID[0].X + PochodnaNPoEta[3][1] * element.ID[1].X + PochodnaNPoEta[3][2] * element.ID[2].X + PochodnaNPoEta[3][3] * element.ID[3].X),
                    PochodnaNPoKsi[3][0] * element.ID[0].X + PochodnaNPoKsi[3][1] * element.ID[1].X + PochodnaNPoKsi[3][2] * element.ID[2].X + PochodnaNPoKsi[3][3] * element.ID[3].X
                    
                }
            };

            double[] wyznaczniki = new double[4]
            {
                jacobian1[0][0]*jacobian1[1][1] - jacobian1[0][1]*jacobian1[1][0],
                jacobian2[0][0]*jacobian2[1][1] - jacobian2[0][1]*jacobian2[1][0],
                jacobian3[0][0]*jacobian3[1][1] - jacobian3[0][1]*jacobian3[1][0],
                jacobian4[0][0]*jacobian4[1][1] - jacobian4[0][1]*jacobian4[1][0]
            };


            //pomnozenie jacobianów razy 1/wyznacznik[J]
            MatrixTimesValue(jacobian1,1.0/wyznaczniki[0]);
            MatrixTimesValue(jacobian2,1.0/wyznaczniki[1]);
            MatrixTimesValue(jacobian3,1.0/wyznaczniki[2]);
            MatrixTimesValue(jacobian4,1.0/wyznaczniki[3]);


            //jakobiany przekształcenia dla podanego elementu
            double[][] pomoc1 = new double[][]
            {
                MatrixTimesVector(jacobian1,new double[]{PochodnaNPoKsi[0][0],PochodnaNPoEta[0][0]}),
                MatrixTimesVector(jacobian1,new double[]{PochodnaNPoKsi[0][1],PochodnaNPoEta[0][1]}),
                MatrixTimesVector(jacobian1,new double[]{PochodnaNPoKsi[0][2],PochodnaNPoEta[0][2]}),
                MatrixTimesVector(jacobian1,new double[]{PochodnaNPoKsi[0][3],PochodnaNPoEta[0][3]}),
            };
            double[][] pomoc2 = new double[][]
            {
                MatrixTimesVector(jacobian2,new double[]{PochodnaNPoKsi[1][0],PochodnaNPoEta[1][0]}),
                MatrixTimesVector(jacobian2,new double[]{PochodnaNPoKsi[1][1],PochodnaNPoEta[1][1]}),
                MatrixTimesVector(jacobian2,new double[]{PochodnaNPoKsi[1][2],PochodnaNPoEta[1][2]}),
                MatrixTimesVector(jacobian2,new double[]{PochodnaNPoKsi[1][3],PochodnaNPoEta[1][3]}),
            };
            double[][] pomoc3 = new double[][]
            {
                MatrixTimesVector(jacobian3,new double[]{PochodnaNPoKsi[2][0], PochodnaNPoEta[2][0]}),
                MatrixTimesVector(jacobian3,new double[]{PochodnaNPoKsi[2][1], PochodnaNPoEta[2][1]}),
                MatrixTimesVector(jacobian3,new double[]{PochodnaNPoKsi[2][2], PochodnaNPoEta[2][2]}),
                MatrixTimesVector(jacobian3,new double[]{PochodnaNPoKsi[2][3], PochodnaNPoEta[2][3]}),
            };
            double[][] pomoc4 = new double[][]
            {
                MatrixTimesVector(jacobian4,new double[]{PochodnaNPoKsi[3][0],PochodnaNPoEta[3][0]}),
                MatrixTimesVector(jacobian4,new double[]{PochodnaNPoKsi[3][1],PochodnaNPoEta[3][1]}),
                MatrixTimesVector(jacobian4,new double[]{PochodnaNPoKsi[3][2],PochodnaNPoEta[3][2]}),
                MatrixTimesVector(jacobian4,new double[]{PochodnaNPoKsi[3][3],PochodnaNPoEta[3][3]})
            };


            //osobne wyniki dla każdego z punktów całkowania
            //wynik1 -> pochodna wektora funkcji kształtu po x razy macierz transponowana + pochodna funkcji kształtu po y razy macierz transponowana
            double[][] wynik1 = MatrixPlusMatrix(VectorTimesTransposedVector(new double[] {pomoc1[0][0],pomoc1[1][0],pomoc1[2][0],pomoc1[3][0] }), 
                VectorTimesTransposedVector(new double[] { pomoc1[0][1], pomoc1[1][1], pomoc1[2][1], pomoc1[3][1] }));
            double[][] wynik2 = MatrixPlusMatrix(VectorTimesTransposedVector(new double[] { pomoc2[0][0], pomoc2[1][0], pomoc2[2][0], pomoc2[3][0] }),
                VectorTimesTransposedVector(new double[] { pomoc2[0][1], pomoc2[1][1], pomoc2[2][1], pomoc2[3][1] }));
            double[][] wynik3 = MatrixPlusMatrix(VectorTimesTransposedVector(new double[] { pomoc3[0][0], pomoc3[1][0], pomoc3[2][0], pomoc3[3][0] }),
                VectorTimesTransposedVector(new double[] { pomoc3[0][1], pomoc3[1][1], pomoc3[2][1], pomoc3[3][1] }));
            double[][] wynik4 = MatrixPlusMatrix(VectorTimesTransposedVector(new double[] { pomoc4[0][0], pomoc4[1][0], pomoc4[2][0], pomoc4[3][0] }),
                VectorTimesTransposedVector(new double[] { pomoc4[0][1], pomoc4[1][1], pomoc4[2][1], pomoc4[3][1] }));

            //zsumowanie wszystkich 4 wyników i zwrócenie rezultatu
            double[][] finalHelp1 = MatrixPlusMatrix(wynik1, wynik2);
            
            double[][] finalHelp2 = MatrixPlusMatrix(wynik3, wynik4);
            //powinno sie wynik zwracajacy pomnożyć razy k(t)



            //Console.WriteLine(finalHelp1.Length);

            return MatrixPlusMatrix(finalHelp1, finalHelp2);
        }




        //funkcje pomocnicze
        public static double[][] VectorTimesTransposedVector(double[] Vector)
        {
            double[] TransVector = new double[Vector.Length];
            TransVector = Vector;
            return VerticalVectorTimesHorizontalVector(Vector, TransVector);
        }

        //Attention This function is modyfing original array!!!
        public static void TransposeMatrix(double[][] Matrix)
        {
            double t = 0;
            for (int i = 0; i < Matrix.Length - 1; i++)
            {
                for (int j = i + 1; j < Matrix.Length; j++)
                {
                    t = Matrix[i][j];
                    Matrix[i][j] = Matrix[j][i];
                    Matrix[j][i] = t;
                }
            }
        }

        public static double[][] MatrixTimesMatrix(double[][] Matrix1, double[][] Matrix2)
        {
            double[][] wynik = new double[Matrix1.Length][];
            double suma = 0;
            for (int i = 0; i < Matrix1.Length; i++)
            {
                wynik[i] = new double[Matrix1.Length];
                for (int j = 0; j < Matrix1.Length; j++)
                {
                    suma = 0;
                    for (int k = 0; k < Matrix1.Length; k++)
                        suma += Matrix1[i][k] * Matrix2[k][j];
                    wynik[i][j] = suma;
                }
            }
            return wynik;
        }
        public static double[][] VerticalVectorTimesHorizontalVector(double[] Matrix1, double[] Matrix2)
        {
            double[][] wynik = new double[4][];

            for (int i = 0; i < 4; i++)
            {
                wynik[i] = new double[4];
                for (int j = 0; j < 4; j++)
                {

                    wynik[i][j] = Matrix1[i]*Matrix2[j];
                }
            }
            return wynik;
        }

        public static double[][] MatrixPlusMatrix(double[][] Matrix1, double[][] Matrix2)
        {
            double[][] wynik = new double[Matrix1.Length][];
            double suma = 0;
            for (int i = 0; i < Matrix1.Length; i++)
            {
                wynik[i] = new double[Matrix1.Length];
                for (int j = 0; j < Matrix1.Length; j++)
                {
                    suma = 0;
                    for (int k = 0; k < Matrix1.Length; k++)
                        suma += Matrix1[i][k] + Matrix2[k][j];
                    wynik[i][j] = suma;
                }
            }
            return wynik;
        }
        public static double[] MatrixTimesVector(double[][] Matrix, double[] Vector)
        {
            double[] wynik = new double[Vector.Length];
            for (int j = 0; j < Matrix.Length; j++)
            {
                for (int k = 0; k < Matrix.Length; k++)
                {
                    wynik[j] += Matrix[j][k] * Vector[k];
                }
            }
            return wynik;
        }
        public static void MatrixTimesValue(double[][] Matrix, double Value)
        {
            for (int i = 0; i < Matrix.Length; i++)
            {
                for (int j = 0; j < Matrix.Length; j++)
                {
                    Matrix[i][j] *= Value;
                }
            }
        }
        public static void PrintMatrix(double[][] Matrix)
        {
            for (int i = 0; i < Matrix.Length; i++)
            {
                for (int j = 0; j < Matrix[i].Length; j++)
                {
                    Console.Write($"{Matrix[i][j]} ");
                }
                Console.WriteLine();
            }
        }
    } 
}
