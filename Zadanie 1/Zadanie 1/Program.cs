﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie_1
{
    class Program
    {
        private readonly double[] _w = {1.0, 1.0};
        private readonly double[] _w2 = { 5.0/9.0, 8.0/9.0,5.0/9.0 };
        private readonly double[] _wsp = {-1.0/Math.Sqrt(3), 1.0 / Math.Sqrt(3) };
        private readonly double[] _wsp2 = {-0.7745, 0, 0.7745};

        static void Main(string[] args)
        {
            var program = new Program();
            program.Count2();
            program.Count3();

            Console.ReadKey();
        }

        private void Count2()
        {
            double wynik = 0.0;
            for (int i = 1; i <= _w.Length; i++)
            {
                for (int j = 1; j <= _w.Length; j++)
                {
                    wynik += Funkcja(_wsp[i-1], _wsp[j-1]) * _w[i-1] * _w[j-1];
                }
            }
            Console.WriteLine($"Wynik: {wynik}");
        }
        private void Count3()
        {
            double wynik = 0.0;
            for (int i = 1; i <= _w2.Length; i++)
            {
                for (int j = 1; j <= _w2.Length; j++)
                {
                      wynik += Funkcja(_wsp2[i - 1], _wsp2[j - 1]) * _w2[i - 1] * _w2[j - 1];
                }
            }
            Console.WriteLine($"Wynik: {wynik}");
        }

        private static double Funkcja(double x, double y)
        {
            return 2 * Math.Pow(x, 2) * Math.Pow(y, 2) + 6 * x + 5;
        }
    }
}